public class Test {
    public static void main(String[] args) {
        for (int arabic = 1; arabic <= 100; arabic++){

            int r1 = arabic % 10;
            int r2 = arabic % 100 / 10;
            int r3 = arabic % 1000 / 100;

            String s1 = "";
            String s2 = "";
            String s3 = "";

            switch (r1) {
                case 1: s1 = "I"; break;
                case 2: s1 = "II"; break;
                case 3: s1 = "III"; break;
                case 4: s1 = "IV"; break;
                case 5: s1 = "V"; break;
                case 6: s1 = "VI"; break;
                case 7: s1 = "VII"; break;
                case 8: s1 = "VIII"; break;
                case 9: s1 = "IX"; break;
            }

            switch (r2) {
                case 1: s2 = "X"; break;
                case 2: s2 = "XX"; break;
                case 3: s2 = "XXX"; break;
                case 4: s2 = "XL"; break;
                case 5: s2 = "L"; break;
                case 6: s2 = "LX"; break;
                case 7: s2 = "LXX"; break;
                case 8: s2 = "LXXX"; break;
                case 9: s2 = "XC"; break;
            }

            switch (r3) {
                case 1: s3 = "C"; break;
                case 2: s3 = "CC"; break;
                case 3: s3 = "CCC"; break;
                case 4: s3 = "CD"; break;
                case 5: s3 = "D"; break;
                case 6: s3 = "DC"; break;
                case 7: s3 = "DCC"; break;
                case 8: s3 = "DCCC"; break;
                case 9: s3 = "CM"; break;
            }

            System.out.println(arabic + " = " + s3 + s2 + s1);
        }
    }
}
